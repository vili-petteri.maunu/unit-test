/**
 * 
 * @param {number} a 
 * @param {number} b 
 * @returns {number}
 */
const add = (a, b) => a + b;

/**
 * 
 * @param {number} a 
 * @param {number} b 
 * @returns {number}
 */
const subtract = (a, b) => a - b;

/**
 * 
 * @param {number} a 
 * @param {number} b 
 * @returns {number}
 */
const multiply = (a, b) => a * b;

/**
 * 
 * @param {number} a 
 * @param {number} b 
 * @returns {number}
 * @throws {Error}
 */
const divide = (a, b) => {
    if (b == 0) throw new Error("0 division not allowed");

    const fraction = a / b;
    return fraction;
};

export default { add, subtract, multiply, divide };
